namespace Foobarqix.Test;


public class BirthdateShould
{
    [Test, TestCaseSource(nameof(Implementations))]
    public void foo_when_divisible_by_3(Func<int, string> fonction)
    {
        Assert.That(fonction(3), Is.EqualTo("foo"));
        Assert.That(fonction(6), Is.EqualTo("foo"));
        Assert.That(fonction(9), Is.EqualTo("foo"));
    }

    [Test, TestCaseSource(nameof(Implementations))]
    public void bar_when_divisible_by_5(Func<int, string> fonction)
    {
        Assert.That(fonction(5), Is.EqualTo("bar"));
        Assert.That(fonction(10), Is.EqualTo("bar"));
        Assert.That(fonction(20), Is.EqualTo("bar"));
    }

    [Test, TestCaseSource(nameof(Implementations))]
    public void qix_when_divisible_by_7(Func<int, string> fonction)
    {
        Assert.That(fonction(7), Is.EqualTo("qix"));
        Assert.That(fonction(14), Is.EqualTo("qix"));
        Assert.That(fonction(28), Is.EqualTo("qix"));
    }

    [Test, TestCaseSource(nameof(Implementations))]
    public void combination_of_foo_bar_qix_when_multiples_divisibility(
        Func<int, string> fonction)
    {
        Assert.That(fonction(3 * 5), Is.EqualTo("foobar"));
        Assert.That(fonction(3 * 7), Is.EqualTo("fooqix"));
        Assert.That(fonction(5 * 7), Is.EqualTo("barqix"));
        Assert.That(fonction(3 * 5 * 7), Is.EqualTo("foobarqix"));
    }

    [Test, TestCaseSource(nameof(Implementations))]
    public void input_otherwise(Func<int, string> fonction)
    {
        Assert.That(fonction(1), Is.EqualTo("1"));
    }

    public static IEnumerable<Func<int, string>> Implementations()
    {
        yield return (value) => FooBarQixImperatif.FooBarQix(value);
        yield return (value) => FooBarQixRecursive.FooBarQix(value);
        yield return (value) => FooBarQixStream.FooBarQix(value);
        yield return (value) => FooBarQixSolid.FooBarQix(value);
    }
}
