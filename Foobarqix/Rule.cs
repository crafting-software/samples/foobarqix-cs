namespace Foobarqix;

public record Rule(int divisor, string label)
{
    public static Rule[] FooBarQixRules()
    {
        return new Rule[] {
            new Rule(3, "foo"),
            new Rule(5, "bar"),
            new Rule(7, "qix"),
        };
    }
}
