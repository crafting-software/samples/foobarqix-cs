﻿namespace Foobarqix;
public class FooBarQixSolid
{
    public static string FooBarQix(int value)
    {
        return FooBarQixInternal(value, Rule.FooBarQixRules());
    }

    private static string FooBarQixInternal(int value, Rule[] rules)
    {
        var result = string.Empty;
        var matched = false;

        foreach (var rule in rules)
        {
            if (value % rule.divisor == 0)
            {
                result += rule.label;
                matched = true;
            }
        }

        return matched ? result : value.ToString();
    }
}
