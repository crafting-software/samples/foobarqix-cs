﻿namespace Foobarqix;

using System.Linq;

public class FooBarQixStream
{
    public static string FooBarQix(int value)
    {
        return FooBarQixStreamed(value, Rule.FooBarQixRules());
    }

    private static string FooBarQixStreamed(int value, Rule[] rules)
    {
        return rules
            .Where((r) => value % r.divisor == 0)
            .Select((r) => r.label)
            .DefaultIfEmpty(value.ToString())
            .Aggregate("", (prev, current) => prev + current);
    }
}
