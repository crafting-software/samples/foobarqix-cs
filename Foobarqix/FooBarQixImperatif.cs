﻿namespace Foobarqix;
public class FooBarQixImperatif
{
    public static string FooBarQix(int value)
    {
        var result = string.Empty;
        if (value % 3 == 0)
            result += "foo";
        if (value % 5 == 0)
            result += "bar";
        if (value % 7 == 0)
            result += "qix";

        if (string.IsNullOrEmpty(result))
            return value.ToString();
        return result;
    }
}
