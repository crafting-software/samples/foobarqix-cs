﻿namespace Foobarqix;
public class FooBarQixRecursive
{
    public static string FooBarQix(int value)
    {
        return FooBarQixR(value, "", false, Rule.FooBarQixRules());
    }

    private static string FooBarQixR(int value, string result, bool matched, Rule[] rules) => rules switch
    {
        [] when matched => result,
        [] => value.ToString(),
        [var head, .. var tail] when value % head.divisor == 0 =>
            FooBarQixR(value, result + head.label, true, tail),
        [var head, .. var tail] => FooBarQixR(value, result, matched, tail),
    };
}
